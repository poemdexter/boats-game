﻿using UnityEngine;
using System.Collections;

public class TerrainGeneration : MonoBehaviour
{
    public GameObject[] tiles;
    private GameObject t1, t2, t3, t4;

    private void Awake()
    {
        PlaceTiles();
    }

    private void PlaceTiles()
    {
        t1 = Instantiate(tiles[Random.Range(0, tiles.Length)], Vector3.zero, Quaternion.identity) as GameObject;
        t2 = Instantiate(tiles[Random.Range(0, tiles.Length)], Vector3.zero, Quaternion.identity) as GameObject;
        t3 = Instantiate(tiles[Random.Range(0, tiles.Length)], Vector3.zero, Quaternion.identity) as GameObject;
        t4 = Instantiate(tiles[Random.Range(0, tiles.Length)], Vector3.zero, Quaternion.identity) as GameObject;

        t1.transform.position = new Vector3(25f, 0f, 25f);
        t2.transform.position = new Vector3(0f, 0f, 25f);
        t3.transform.position = new Vector3(0f, 0f, 0f);
        t4.transform.position = new Vector3(25f, 0f, 0f);

        t1.tag = "Quadrant1";
        t2.tag = "Quadrant2";
        t3.tag = "Quadrant3";
        t4.tag = "Quadrant4";
    }
}