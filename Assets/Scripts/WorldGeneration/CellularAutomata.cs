﻿using System.Collections.Generic;
using UnityEngine;
using System.Collections;

public class CellularAutomata : MonoBehaviour
{
    public int width, height;
    public float terrainPower;

    public float chanceToStartAlive = 0.45f;
    public int deathLimit = 3;
    public int birthLimit = 5;
    public int simulationSteps = 5;
    public float generationOffset = 2.4f;

    private bool[,] cellMap;

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            ClearMap();
            cellMap = new bool[width, height];
            InitializeMap(cellMap);
            for (int i = 0; i < simulationSteps; i++)
            {
                cellMap = DoSimulationStep(cellMap);
            }
            //CloseBorders(cellMap);
            PlaceCells(cellMap);
            RaiseTerrain();
        }
    }

    private void CloseBorders(bool[,] map)
    {
        for (int x = 0; x < width; x++)
        {
            map[x, 0] = true;
            map[x, height - 1] = true;
        }
        for (int y = 0; y < height; y++)
        {
            map[0, y] = true;
            map[width - 1, y] = true;
        }
    }

    private void ClearMap()
    {
        foreach (Transform child in transform)
        {
            Destroy(child.gameObject);
        }
    }

    private void InitializeMap(bool[,] map)
    {
        for (int x = 0; x < width; x++)
        {
            for (int y = 0; y < height; y++)
            {
                if (Random.Range(0f, 1f) < chanceToStartAlive)
                {
                    map[x, y] = true;
                }
            }
        }
    }

    private bool[,] DoSimulationStep(bool[,] map)
    {
        bool[,] simMap = new bool[width, height];

        for (int x = 0; x < width; x++)
        {
            for (int y = 0; y < height; y++)
            {
                int neighbors = CountAliveNeighbors(map, x, y);
                if (map[x, y])
                {
                    // first, if a cell is alive but has too many dead neighbours, kill it.
                    simMap[x, y] = (neighbors < deathLimit) ? false : true;
                }
                else
                {
                    // otherwise, if the cell is dead now, check if it has the right number of neighbors to be born.
                    simMap[x, y] = (neighbors > birthLimit);
                }
            }
        }
        return simMap;
    }

    private int CountAliveNeighbors(bool[,] map, int x, int y)
    {
        int count = 0;
        for (int i = -1; i < 2; i++)
        {
            for (int j = -1; j < 2; j++)
            {
                int neighborX = x + i;
                int neighborY = y + j;
                //If we're looking at the middle point
                if (i == 0 && j == 0)
                {
                    //Do nothing, we don't want to add ourselves in!
                }
                    //In case the index we're looking at it off the edge of the map
                else if (neighborX < 0 || neighborY < 0 || neighborX >= width || neighborY >= height)
                {
                    count = count + 1;
                }
                    //Otherwise, a normal check of the neighbour
                else if (map[neighborX, neighborY])
                {
                    count = count + 1;
                }
            }
        }
        return count;
    }

    private void PlaceCells(bool[,] map)
    {
        MeshFilter meshFilter = GetComponent<MeshFilter>();
        if (meshFilter == null)
        {
            Debug.LogError("MeshFilter not found!");
            return;
        }

        List<Vector3> points = new List<Vector3>();
        List<Vector3> verts = new List<Vector3>();
        List<int> tris = new List<int>();

        for (int x = 0; x < width; x++)
        {
            for (int y = 0; y < height; y++)
            {
                if (map[x, y])
                {
                    // create new points with quad
                    points.Add(new Vector3(x, 0, y));
                    points.Add(new Vector3(x, 0, y + 1));
                    points.Add(new Vector3(x + 1, 0, y + 1));
                    points.Add(new Vector3(x + 1, 0, y));

                    // add top verts for tri
                    verts.Add(points[points.Count - 4]);
                    verts.Add(points[points.Count - 3]);
                    verts.Add(points[points.Count - 2]);

                    // add bottom verts for tri
                    verts.Add(points[points.Count - 4]);
                    verts.Add(points[points.Count - 2]);
                    verts.Add(points[points.Count - 1]);
                }
            }
        }

        // create the triangles
        for (int i = 0; i < verts.Count; i++)
        {
            tris.Add(i);
        }

        meshFilter.mesh.vertices = verts.ToArray();
        meshFilter.mesh.triangles = tris.ToArray();
        meshFilter.mesh.RecalculateNormals();
        meshFilter.mesh.RecalculateBounds();
        meshFilter.mesh.Optimize();
    }

    private void RaiseTerrain()
    {
        Mesh mesh = GetComponent<MeshFilter>().mesh;
        Vector3[] baseHeight = mesh.vertices;
        Vector3[] vertices = new Vector3[baseHeight.Length];

        for (var i = 0; i < vertices.Length; i++)
        {
            Vector3 vertex = baseHeight[i];

            Debug.Log(vertex.x + " " + vertex.z + " " + Mathf.PerlinNoise(vertex.x + 0.3f, vertex.z + 0.3f));
            vertex.y = Mathf.PerlinNoise(vertex.x + 0.3f, vertex.z + 0.1f) * terrainPower;

            vertices[i] = vertex;
        }
        mesh.vertices = vertices;
        mesh.RecalculateNormals();

        GetComponent<MeshFilter>().mesh = mesh;
    }
}