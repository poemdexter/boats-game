﻿using UnityEngine;
using System.Collections;

public class SineWaves : MonoBehaviour
{
    private float currentTime;

    private void Update()
    {
        Mesh mesh = GetComponent<MeshFilter>().mesh;
        Vector3[] baseHeight = mesh.vertices;
        Vector3[] vertices = new Vector3[baseHeight.Length];

        currentTime += Time.deltaTime;

        for (var i = 0; i < vertices.Length; i++)
        {
            Vector3 vertex = baseHeight[i];
            vertex.y = .1f * Mathf.Sin(currentTime + .2f + (vertex.z * 1)) +
                       .1f * Mathf.Sin(currentTime + (vertex.x * 1));
            vertices[i] = vertex;
        }
        mesh.vertices = vertices;
        mesh.RecalculateNormals();

        GetComponent<MeshFilter>().mesh = mesh;
    }
}