﻿using UnityEngine;
using System.Collections;

public class AIPoint
{
    public NavPoint Position { get; set; }
    public NavPoint Parent { get; set; }
    public float G { get; set; } // cost to move to this square
    public float H { get; set; } // distance to goal

    public float F
    {
        get { return G + H; }
    }

    public AIPoint(NavPoint pos, NavPoint prnt, float g, float h)
    {
        this.Position = pos;
        this.Parent = prnt;
        this.G = g;
        this.H = h;
    }
}