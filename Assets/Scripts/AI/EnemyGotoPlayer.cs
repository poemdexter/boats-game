﻿using System.Collections.Generic;
using UnityEngine;

public class EnemyGotoPlayer : MonoBehaviour
{
    private PathfindingManager pathManager;
    private Stack<NavPoint> waypoints;
    private NavPoint currentNavPoint;
    public float searchDelay;
    private float currentTime;

    private void Start()
    {
        pathManager = GameObject.FindGameObjectWithTag("Pathfinding").GetComponent<PathfindingManager>();
    }

    private void Update()
    {
        if ((currentTime += Time.deltaTime) > searchDelay)
        {
            currentTime = 0f;
            waypoints = pathManager.GetRouteToPlayer(pathManager.GetNearestPointToSelf(transform.position));
        }

        if (waypoints != null && waypoints.Count > 0)
        {
            GotoPlayer();
        }
    }

    private void GotoPlayer()
    {
        if (currentNavPoint == null && waypoints.Count > 0)
        {
            currentNavPoint = waypoints.Pop();
            transform.LookAt(currentNavPoint.GetVector3(pathManager.GetNodeOffsets()));
            Debug.Log("Going to " + currentNavPoint.GetVector3(pathManager.GetNodeOffsets()));
        }

        if (currentNavPoint != null)
        {
            transform.Translate(Vector3.forward * Time.deltaTime * 2f);

            if (Vector3.Distance(currentNavPoint.GetVector3(pathManager.GetNodeOffsets()), transform.position) < .5f)
            {
                currentNavPoint = null;
            }
        }
    }
}