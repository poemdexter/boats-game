﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using System.Collections;

public class PathfindingManager : MonoBehaviour
{
    public int worldNavArrayWidth, worldNavArrayHeight;
    public float xOriginOffset, zOriginOffset;
    public float xNodeDistance, zNodeDistance;
    public int tileRowNodeCount;

    private NavGridArray q1, q2, q3, q4;
    private bool[,] worldNavArray;

    private void Start()
    {
        q1 = GameObject.FindGameObjectWithTag("Quadrant1").transform.FindChild("NavGrid").GetComponent<NavGridArray>();
        q2 = GameObject.FindGameObjectWithTag("Quadrant2").transform.FindChild("NavGrid").GetComponent<NavGridArray>();
        q3 = GameObject.FindGameObjectWithTag("Quadrant3").transform.FindChild("NavGrid").GetComponent<NavGridArray>();
        q4 = GameObject.FindGameObjectWithTag("Quadrant4").transform.FindChild("NavGrid").GetComponent<NavGridArray>();

        BuildWorldNavArray();
        GetNearestPointToPlayer();
    }

    public float[] GetNodeOffsets()
    {
        return new float[] {xOriginOffset, zOriginOffset, xNodeDistance, zNodeDistance};
    }

    private void BuildWorldNavArray()
    {
        worldNavArray = new bool[worldNavArrayWidth, worldNavArrayHeight];

        // quadrant 3
        for (int y = 0; y < tileRowNodeCount; y++)
        {
            for (int x = 0; x < tileRowNodeCount; x++)
            {
                worldNavArray[x, y] = q3.points[(y * tileRowNodeCount) + x].valid;
            }
        }

        // quadrant 4
        for (int y = 0; y < tileRowNodeCount; y++)
        {
            for (int x = 0; x < tileRowNodeCount; x++)
            {
                worldNavArray[tileRowNodeCount + x, y] = q4.points[(y * tileRowNodeCount) + x].valid;
            }
        }

        // quadrant 2
        for (int y = 0; y < tileRowNodeCount; y++)
        {
            for (int x = 0; x < tileRowNodeCount; x++)
            {
                worldNavArray[x, tileRowNodeCount + y] = q2.points[(y * tileRowNodeCount) + x].valid;
            }
        }

        // quadrant 1
        for (int y = 0; y < tileRowNodeCount; y++)
        {
            for (int x = 0; x < tileRowNodeCount; x++)
            {
                worldNavArray[tileRowNodeCount + x, tileRowNodeCount + y] = q1.points[(y * tileRowNodeCount) + x].valid;
            }
        }
    }

    public Stack<NavPoint> GetRouteToPlayer(NavPoint enemyPosition)
    {
        // gotta get A* path to player if possible
        // http://www.policyalmanac.org/games/aStarTutorial.htm
        // following those steps, just going to comment step numbers for ease

        NavPoint playerCoords = GetNearestPointToPlayer();

        List<AIPoint> openList = new List<AIPoint>();
        List<AIPoint> closedList = new List<AIPoint>();

        // 1
        AIPoint startPoint = new AIPoint(enemyPosition, enemyPosition, 0, 0);
        openList.Add(startPoint);

        bool playerFound = false;

        while (openList.Count != 0 && !playerFound)
        {
            // 4
            AIPoint parentAI = openList[0];
            openList.Remove(parentAI);
            closedList.Add(parentAI);

            // player position found
            if (parentAI.Position.Equals(playerCoords))
            {
                playerFound = true;
                break;
            }

            NavPoint parent = new NavPoint(parentAI.Position.X, parentAI.Position.Y);
            // 2
            // checking in this order
            // 2 3 4
            // 1 X 5
            // 8 7 6
            if (parent.X - 1 >= 0 && worldNavArray[parent.X - 1, parent.Y]) // 1
            {
                NavPoint looking = new NavPoint(parent.X - 1, parent.Y);
                HandleNodeCheck(looking, parent, playerCoords, parentAI, closedList, openList,1f);
            }
            if (parent.X - 1 >= 0 && parent.Y + 1 < worldNavArrayHeight && worldNavArray[parent.X - 1, parent.Y + 1]) // 2
            {
                NavPoint looking = new NavPoint(parent.X - 1, parent.Y + 1);
                HandleNodeCheck(looking, parent, playerCoords, parentAI, closedList, openList,1.4f);
            }
            if (parent.Y + 1 < worldNavArrayHeight && worldNavArray[parent.X, parent.Y + 1]) // 3
            {
                NavPoint looking = new NavPoint(parent.X , parent.Y + 1);
                HandleNodeCheck(looking, parent, playerCoords, parentAI, closedList, openList,1f);
            }
            if (parent.X + 1 < worldNavArrayWidth && parent.Y + 1 < worldNavArrayHeight && worldNavArray[parent.X + 1, parent.Y + 1]) // 4
            {
                NavPoint looking = new NavPoint(parent.X + 1, parent.Y + 1);
                HandleNodeCheck(looking, parent, playerCoords, parentAI, closedList, openList,1.4f);
            }
            if (parent.X + 1 < worldNavArrayWidth && worldNavArray[parent.X + 1, parent.Y]) // 5
            {
                NavPoint looking = new NavPoint(parent.X + 1, parent.Y);
                HandleNodeCheck(looking, parent, playerCoords, parentAI, closedList, openList,1f);
            }
            if (parent.X + 1 < worldNavArrayWidth && parent.Y - 1 >= 0 && worldNavArray[parent.X + 1, parent.Y - 1]) // 6
            {
                NavPoint looking = new NavPoint(parent.X + 1, parent.Y - 1);
                HandleNodeCheck(looking, parent, playerCoords, parentAI, closedList, openList,1.4f);
            }
            if (parent.Y - 1 >= 0 && worldNavArray[parent.X, parent.Y - 1]) // 7
            {
                NavPoint looking = new NavPoint(parent.X, parent.Y - 1);
                HandleNodeCheck(looking, parent, playerCoords, parentAI, closedList, openList,1f);
            }
            if (parent.X - 1 >= 0 && parent.Y - 1 >= 0 && worldNavArray[parent.X - 1, parent.Y - 1]) // 8
            {
                NavPoint looking = new NavPoint(parent.X - 1, parent.Y - 1);
                HandleNodeCheck(looking, parent, playerCoords, parentAI, closedList, openList,1.4f);
            }

            // 3
            openList = openList.OrderBy(x => x.F).ToList();
        }

        // get path now
        Stack<NavPoint> pointStack = new Stack<NavPoint>();
        if (playerFound)
        {
            // backtrack through closed list parents to get path
            AIPoint start = GetAiPoint(playerCoords, closedList);
            
            bool pathing = true;
            while (pathing)
            {
                if (start.Parent.Equals(startPoint.Position))
                {
                    pathing = false;
                }
                else
                {
                    pointStack.Push(start.Position);
                    start = GetAiPoint(start.Parent, closedList);
                }
            }
        }
        Debug.Log(pointStack.Count);
        return pointStack;
    }

    private void HandleNodeCheck(NavPoint looking, NavPoint parent, NavPoint playerCoords, AIPoint parentAI, List<AIPoint> closedList, List<AIPoint> openList, float moveAmt)
    {
        if (!IsInList(looking, closedList)) // 5
        {
            if (!IsInList(looking, openList)) // 6
            {
                openList.Add(new AIPoint(looking, parent, parentAI.G + moveAmt, getH(looking, playerCoords)));
            }
            else
            {
                AIPoint pathCheckPoint = GetAiPoint(looking, openList);
                if (pathCheckPoint.G > parentAI.G + moveAmt)
                {
                    pathCheckPoint.Parent = parent;
                }
            }
        }
    }

    private NavPoint GetNearestPointToPlayer()
    {
        Vector3 playerPos = GameObject.FindGameObjectWithTag("Player").transform.position;

        float x = playerPos.x - xOriginOffset;
        float z = playerPos.z - zOriginOffset;

        float px = Mathf.Round(x / xNodeDistance);
        float pz = Mathf.Round(z / xNodeDistance);

        return new NavPoint(px,pz);
    }

    public NavPoint GetNearestPointToSelf(Vector3 position)
    {

        float x = position.x - xOriginOffset;
        float z = position.z - zOriginOffset;

        float px = Mathf.Round(x / xNodeDistance);
        float pz = Mathf.Round(z / xNodeDistance);

        return new NavPoint(px, pz);
    }

    // checks if AIPoint exists in List by checking positions
    private bool IsInList(NavPoint v, List<AIPoint> list)
    {
        foreach (AIPoint point in list)
        {
            if (point.Position == v)
                return true;
        }
        return false;
    }

    // grab the AIPoint based on vector
    private AIPoint GetAiPoint(NavPoint v, List<AIPoint> list)
    {
        foreach (AIPoint point in list)
        {
            if (point.Position.Equals(v))
                return point;
        }
        return null;
    }

    // gets manhattan distance from mob to player
    private int getH(NavPoint mob, NavPoint player)
    {
        return (int)(Mathf.Abs(mob.X - player.X) + Mathf.Abs(mob.Y - player.Y));
    }
}