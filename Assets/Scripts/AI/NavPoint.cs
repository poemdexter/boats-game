﻿using UnityEngine;
using System.Collections;

public class NavPoint
{
    public int X { get; set; }
    public int Y { get; set; }

    public NavPoint(int x, int y)
    {
        X = x;
        Y = y;
    }

    public NavPoint(float x, float y)
    {
        X = Mathf.RoundToInt(x);
        Y = Mathf.RoundToInt(y);
    }

    public bool Equals(NavPoint point)
    {
        return point.X == X && point.Y == Y;
    }

    public Vector3 GetVector3(float[] offsets)
    {
        return new Vector3(offsets[0] + (X * offsets[2]), 0.055f, offsets[1] + (Y * offsets[3]));
    }
}
