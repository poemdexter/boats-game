﻿using System.Security.Permissions;
using UnityEngine;
using System.Collections;

public class ChangeCameraViewInput : MonoBehaviour
{
    public float rotateAxisTrigger;
    public float controllerRotateSpeed;
    public float mouseRotateSpeed;
    public float zoomAxisTrigger;
    public float zoomSpeed;
    public float zoomMin, zoomMax;

    private bool mouseRightClick;
    private Vector3 lastMousePosition;

    private void Update()
    {
        float xInput = Input.GetAxisRaw("R_XAxis_1");
        float yInput = Input.GetAxisRaw("R_YAxis_1");

        if (Input.GetAxis("Mouse ScrollWheel") < 0) // back
        {
            yInput = 1f;
        }
        else if (Input.GetAxis("Mouse ScrollWheel") > 0) // forward
        {
            yInput = -1f;
        }

        if (xInput > rotateAxisTrigger || xInput < -rotateAxisTrigger)
        {
            // rotate around the ship
            transform.RotateAround(transform.parent.position, Vector3.up, xInput * controllerRotateSpeed);
        }

        if (yInput > zoomAxisTrigger || yInput < -zoomAxisTrigger)
        {
            if (transform.localPosition.y + yInput < zoomMax && transform.localPosition.y + yInput > zoomMin)
            {
                transform.Translate(Vector3.forward * zoomSpeed * -yInput); // flip sign
            }
        }

        if (Input.GetMouseButtonDown(1)) // right click
        {
            mouseRightClick = true;
            lastMousePosition = Vector3.zero;
        }

        if (Input.GetMouseButtonUp(1)) // right click off
        {
            mouseRightClick = false;
        }

        if (mouseRightClick) // mouse move camera
        {
            if (lastMousePosition != Vector3.zero) // prevent first click from moving camera
            {
                Vector3 delta = Input.mousePosition - lastMousePosition;
                transform.RotateAround(transform.parent.position, Vector3.up, delta.x * mouseRotateSpeed);
            }
            lastMousePosition = Input.mousePosition;
        }
    }
}