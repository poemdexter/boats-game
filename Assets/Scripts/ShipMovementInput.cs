﻿using UnityEngine;
using System.Collections;

public enum MoveSpeed
{
    None,
    Slow,
    Med,
    Fast
}

public class ShipMovementInput : MonoBehaviour
{
    public float moveSpeed1;
    public float moveSpeed2;
    public float moveSpeed3;
    public float turnSpeed;

    private MoveSpeed currentSpeed = MoveSpeed.None;
    private bool turningLeft, turningRight;

    private void Update()
    {
        float xInput = Input.GetAxis("L_XAxis_1");

        if (Input.GetButtonDown("A_1") || Input.GetKeyDown(KeyCode.W))
        {
            IncreaseSpeed();
        }

        if (Input.GetButtonDown("B_1") || Input.GetKeyDown(KeyCode.S))
        {
            DecreaseSpeed();
        }

        // translation
        transform.Translate(Vector3.forward*GetCurrentSpeed()*Time.deltaTime);
        
        // rotation
        if (xInput != 0)
            transform.Rotate(Vector3.up*turnSpeed*Time.deltaTime*xInput);

        if (Input.GetKeyDown(KeyCode.A))
        {
            turningLeft = true;
        }
            
        else
        if (Input.GetKeyDown(KeyCode.D))
        {
            turningRight = true;
        }

        if (Input.GetKeyUp(KeyCode.A))
        {
            turningLeft = false;
        }

        else
        if (Input.GetKeyUp(KeyCode.D))
        {
            turningRight = false;
        }
        
        if (turningLeft)
            transform.Rotate(Vector3.up * turnSpeed * Time.deltaTime * -1f);
        else if (turningRight)
            transform.Rotate(Vector3.up * turnSpeed * Time.deltaTime * 1f);
    }

    private void IncreaseSpeed()
    {
        switch (currentSpeed)
        {
            case MoveSpeed.None:
                currentSpeed = MoveSpeed.Slow;
                break;
            case MoveSpeed.Slow:
                currentSpeed = MoveSpeed.Med;
                break;
            case MoveSpeed.Med:
                currentSpeed = MoveSpeed.Fast;
                break;
        }
    }

    private void DecreaseSpeed()
    {
        switch (currentSpeed)
        {
            case MoveSpeed.Slow:
                currentSpeed = MoveSpeed.None;
                break;
            case MoveSpeed.Med:
                currentSpeed = MoveSpeed.Slow;
                break;
            case MoveSpeed.Fast:
                currentSpeed = MoveSpeed.Med;
                break;
        }
    }

    private float GetCurrentSpeed()
    {
        switch (currentSpeed)
        {
            case MoveSpeed.Slow:
                return moveSpeed1;
            case MoveSpeed.Med:
                return moveSpeed2;
            case MoveSpeed.Fast:
                return moveSpeed3;
        }
        return 0;
    }
}